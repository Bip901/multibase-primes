class Program
{
    static void Main(string[] args)
    {
        ulong num = 0b1; //The search starts at this binary number. Make sure that's an odd number!
        while (true)
        {
            if (
                IsPrime(num) &&
                IsPrime(FromBase2(num, 10)) &&
                IsPrime(FromBase2(num, 3)) &&
                IsPrime(FromBase2(num, 4)) &&
                IsPrime(FromBase2(num, 5)) &&
                IsPrime(FromBase2(num, 6))
                )
            {
                Console.WriteLine("Found number! Binary: " + Convert.ToString((long)num, 2));
                Console.WriteLine("Decimal: " + num);
                Console.WriteLine("Enter anything to keep searching");
                Console.ReadLine();
            }
            num += 2;
        }
    }

    //Returns the number "n" (which is written in base 2) as if it was written in base "newBase".
    private static ulong FromBase2(ulong n, ulong newBase)
    {
        ulong result = 0;
        ulong mult = 1;
        while (n > 0)
        {
            result += mult * (n & 1);
            n >>= 1;
            mult *= newBase;
        }
        return result;
    }

    //Returns whether the number n (n > 1) is prime.
    private static bool IsPrime(ulong n)
    {
        if ((n % 2) == 0) return false;
        ulong max = (ulong)Math.Sqrt(n);
        for (ulong i = 3; i <= max; i += 2)
        {
            if (n % i == 0) return false;
        }
        return true;
    }
}